#include "RequestedServer.h"

#include <sstream>

RequestedServer::RequestedServer(int id, string ip, string port, string fileName){

  _id            = id;
  _ip            = ip;
  _port          = port;
  _fileName      = fileName;
  _fileContent   = "";
  _currCommand   = "";
  _unsentCommand = "";
  _socketfd      = -1;
  _status        = 0;

}

void RequestedServer::showInfo()
{
  cout << "id: " << _id << endl
    << "ip: " << _ip << endl
    << "port: " << _port << endl
    << "fileName: " << _fileName << endl
    << "fileContent: " << _fileContent << endl
    << "socketfd: " << _socketfd << endl
    << "status: " << _status << endl;
}

// split string into list array by delim
list<string> RequestedServer::split(string str, char delimiter) {
  list<string> internal;
  // Turn the string into a stream.
  stringstream ss(str); 
  string tok;

  while(getline(ss, tok, delimiter)) {
    internal.push_back(tok);
  }
  return internal;
}

void RequestedServer::setSocketfd(int socketfd)
{
  _socketfd = socketfd;
}

int RequestedServer::getSocketfd()
{
  return _socketfd;
}


void RequestedServer::setStatus(int status)
{
  _status = status;
}

int RequestedServer::getStatus()
{
  return _status;
}

void RequestedServer::setUnsentCommand(string unsentCommand)
{
  _unsentCommand = unsentCommand;
}

string RequestedServer::getUnsentCommand()
{
  return _unsentCommand;
}

void RequestedServer::setFileContent(string fileContent)
{
  _fileContent = fileContent;
  _commandList = split(fileContent, '\n');
}

string RequestedServer::getFileContent()
{
  return _fileContent;
}

string RequestedServer::getIP()
{
  return _ip;
}

int RequestedServer::getID()
{
  return _id;
}

int RequestedServer::hasCommand()
{
  if(!_commandList.empty()){
    _currCommand = _commandList.front() + "\n";
    _unsentCommand = _currCommand;
    _commandList.pop_front();
    return 1;
  }
  return 0;
}

