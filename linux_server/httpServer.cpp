#include <iostream>
#include <regex>
#include <fstream>
#include <vector>
#include <sstream>
#include <sys/wait.h>
#include "socketFunc.cpp"

void startHttpServer(char*);
void handleRequest(int);
string getHTMLFile(string);
void response(string, string);

int main(int argc, char *argv[])
{
  char* port = (char*) "2015";
  if (argv[1] != NULL){
    port = argv[1];
  }

  startHttpServer(port);
  return 0;
}

vector<string> split(string str, char delimiter) {
  vector<string> internal;
  // Turn the string into a stream.
  stringstream ss(str); 
  string tok;

  while(getline(ss, tok, delimiter)) {
    internal.push_back(tok);
  }
  return internal;
}

void startHttpServer(char* port)
{

  struct sockaddr_in fsin;
  int alen;

  int msock = passiveTCP(port, 40);
  int ssock;
  cout << msock <<endl;

  while(1){
    ssock = accept(msock, (struct sockaddr *)&fsin, (socklen_t *)&alen);
    cout << "ssock:" << ssock << endl;
    // cout << "accept\n";
    if(ssock < 0){
      cerr << "Accept Error\n";
      exit(0);
    }

    switch (fork()) {
      case 0:
        // children process
        close(msock);
        handleRequest(ssock);
        
        exit(1);
      default:
        // parent proces
        close(ssock);
        sleep(1);
        break;
      case -1:
        exit(0);
    }
  }
}

// handle the http request.
void handleRequest(int ssock)
{
  dup2(ssock, STDOUT_FILENO);

  // get http request
  // remove line break in http request.
  regex cleanRegex("[\r\n]");
  string requestString = string(readLineFromSocket(ssock));
  requestString = regex_replace(requestString, cleanRegex, "");

  // get request parameter by regex.
  regex re("^(\\w+) ([\\/\\w\\.\\?\\=\\&]*) (HTTP\\/\\d\\.\\d).*");
  smatch sm;
  regex_match(requestString, sm, re); 


  // cerr << requestString << endl;
  if(sm.size() < 4)
    return;

  cout << "HTTP/1.0 200 OK\n";

  string fileName; 
  string queryParameterString;

  string parameterString = string(sm[2]);

  // cerr << "request string: " << parameterString << endl;

  // parse request string 
  // if request has parameter(GET) get the parameters
  if(parameterString.find("?") != string::npos){
    vector<string> parameter = split(parameterString, '?');
    fileName = parameter[0];
    queryParameterString = parameter[1];
  } else {
    // get the file name without parameteres.
    fileName = parameterString;
    queryParameterString = "";
  }

  // response to client browser.
  response(fileName, queryParameterString);
  exit(1);
}


void response(string fileName, string paramString)
{
  setenv("QUERY_STRING", paramString.c_str(), 1);

  // get the vice file name.
  vector<string> fileInfo = split(fileName, '.');

  // add local path to the file.
  fileName = "." + fileName;
  cerr << fileName << endl;

  if(fileInfo[1] == "html"){
    cout << "Content-Type:text/html;charset=big5\n\n";
    cout << getHTMLFile(fileName);
  }
  else if(fileInfo[1] == "cgi"){

    int pid = fork();
    if(pid == 0){
      cerr << "Execute CGI: " << fileName;

      int exeResult = execl(fileName.c_str(), fileName.c_str());
      cerr << "execResult: " << exeResult;
      exit(0);
    } else {
      
      // wait for child complete.
      int status;
      wait(&status);
      exit(1);
    }
  }
}

// read html file to string.
string getHTMLFile(string fileName)
{
  ifstream fileStream(fileName.c_str());
  // read whole file content by ifstream.
  string content((istreambuf_iterator<char>(fileStream)),
      (istreambuf_iterator<char>()));
  fileStream.close();
  return content;
}
