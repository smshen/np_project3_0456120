#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <regex>
#include <vector>
#include <fstream>

#include "socketFunc.cpp"
#include "RequestedServer.cpp"

using namespace std;

#define F_CONNECTING 0
#define F_READING 1
#define F_WRITING 2
#define F_DONE 3

vector<RequestedServer> serverList;

char* readLine(int);
string cleanString(string);
void getServerInfo();
void parseQueryString();
void connect2Server();
void writeHTMLTemplate();
void writeTable();


int main(int argc, char *argv[], char *envp[])
{
  cout << "Content-Type:text/html;charset=big5\n\n";

  writeHTMLTemplate();

  parseQueryString();

  writeTable();

  connect2Server();

  cout 
    << "</font>"
    << "</body>"
    << "</html>";
  return 0;
}


// replace '\r' to ''.
string cleanString(string targetString)
{

  regex e1 ("[\r]");
  return regex_replace(targetString, e1, "");

}

void writeHTMLTemplate()
{
  cout
    << "<html>"
    << "<head>"
    << "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=big5\" />"
    << "<title>Network Programming Homework 3</title>"
    << "</head>"
    << "<body bgcolor=#336699>"
    << "<font face=\"Courier New\" size=2 color=#FFFF99>"
    << "<table width=\"800\" border=\"1\">";

}

void write2Browser(int id, string msg)
{
  string tempString;

  // replace '>' to '&gt' for html syntax.
  regex e2 (">");
  tempString = regex_replace(cleanString(msg), e2, "&gt");

  // replace '<' to '&lt' for html syntax.
  regex e3 ("<");
  tempString = regex_replace(tempString, e3, "&lt");

  // replace '\n' to '<br>' for html syntax.
  regex e("[\n]");
  tempString = regex_replace(tempString, e, "<br>");

  cout << "<script>document.all[\'m" + to_string(id) + "\'].innerHTML += \"" + tempString + "\";</script>";
  cout.flush();

}

void parseQueryString()
{
  // get the request string from GET
  char *query = getenv("QUERY_STRING");
  string queryString = string(query);

  // use regex to get server information.
  regex re ("h1=(.*)&p1=(.*)&f1=(.*)&h2=(.*)&p2=(.*)&f2=(.*)&h3=(.*)&p3=(.*)&f3=(.*)&h4=(.*)&p4=(.*)&f4=(.*)&h5=(.*)&p5=(.*)&f5=(.*)");
  smatch sm;

  // get match string by regular expression.
  regex_match(queryString, sm, re); 

  int i;
  // retrieve server information from query string
  for (i = 0; i < sm.size(); ++i) {
    if((i % 3) == 1){

      // get the information of server.
      int id          = i / 3;
      string ip       = sm[i];
      string port     = sm[++i];
      string fileName = "./test/" + string(sm[++i]);

      // if no missing information
      if(ip != "" && port != "" && fileName != ""){

        RequestedServer server = RequestedServer(id, ip, port, fileName);

        // check socket and file is valid.
        int socketfd = connectTCP(ip.c_str(), port.c_str());
        ifstream fileStream(fileName.c_str());

        // if socket and file is valid add server to list.
        if((socketfd > 0) && (fileStream.is_open())){

          // get the content of file.
          string content((istreambuf_iterator<char>(fileStream)),
              (istreambuf_iterator<char>()));
          
          // set socket and file conetent to the server.
          server.setFileContent(cleanString(content));
          server.setSocketfd(socketfd);

          serverList.push_back(server);

        }
        // close file stream.
        fileStream.close();
      }

    }
  }
}


// write the html table depend on the number of valid requested servers.
void writeTable()
{
  // write table header.
  cout << "<tr>";
  int i;
  for (i = 0; i < serverList.size(); ++i) {
    cout << "<td>" << serverList[i].getIP() << "</td>";
  }

  cout << "</tr><tr>";

  // write table content.
  for (i = 0; i < serverList.size(); ++i) {
    cout << "<td valign=\"top\" id=\"m" << serverList[i].getID() << "\"></td>";
  }
  cout << "</tr>"
    << "</table>";
}

// connect to remote shell to get result
void connect2Server()
{

  // readable file descriptors
  fd_set rfds;
  // writable file descriptors
  fd_set wfds;
  // active file descriptors
  fd_set rs;
  // active file descriptors
  fd_set ws;

  int conn = serverList.size();
  int nfds,fd;
    
  nfds = FD_SETSIZE;

  // initialize all fd set.
  FD_ZERO(&rfds);
  FD_ZERO(&wfds);
  FD_ZERO(&rs);
  FD_ZERO(&ws);
  
  // set valid socket to fd set.
  int i;
  for (i = 0; i < serverList.size(); ++i) {
    FD_SET(serverList[i].getSocketfd(), &rs);
    // FD_SET(serverList[i].getSocketfd(), &ws);
  }

  rfds = rs;
  wfds = ws;
  
  while(conn > 0){
    memcpy(&rfds, &rs, sizeof(rfds));
    memcpy(&wfds, &ws, sizeof(wfds));

    if(select(nfds, &rfds, &wfds, NULL, NULL) < 0)
      exit(0);

    // iterate all server to check status.
    for (i = 0; i < serverList.size(); ++i) {

      RequestedServer *selectedServer = &(serverList[i]); 
      int selectedSocket = selectedServer->getSocketfd();
      int status = selectedServer->getStatus();

      // if server is just connected.
      if(status == F_CONNECTING && (FD_ISSET(selectedSocket, &rfds) || FD_ISSET(selectedSocket, &wfds))){
        selectedServer->setStatus(F_READING);
      } 

      // if server need read.
      if(status == F_READING && FD_ISSET(selectedSocket, &rfds)){

        int n;
        char buffer[MAX_LENGTH_PER_CMD_LINE];
        memset(buffer, '\0', sizeof(buffer));
        n = read(selectedSocket, buffer, MAX_LENGTH_PER_CMD_LINE);
        write2Browser(selectedServer->getID(), string(buffer));

        // if socket is closed, remove the conn.
        if(n == 0){
          conn--;
          FD_CLR(selectedSocket, &rs);
          close(selectedSocket);
        } else if(string(buffer).find("% ") != string::npos){
          // if receive the normal complete command promote, set fd to write the next command.
          selectedServer->setStatus(F_WRITING);
          FD_CLR(selectedSocket, &rs);
          FD_SET(selectedSocket, &ws);
        }
      }

      // if server can write.
      if(status == F_WRITING && FD_ISSET(selectedSocket, &wfds)){

        // check no unsent command then get next command.
        if(selectedServer->getUnsentCommand() == ""){
          if(!selectedServer->hasCommand()){
            // if no more command need to be sent, clear write fd set.
            FD_CLR(selectedSocket, &ws);
          }
        }

        // sent command.
        string unsentCommand = selectedServer->getUnsentCommand();
        write2Browser(selectedServer->getID(), cleanString(unsentCommand));
        int sentCount = write(selectedSocket, unsentCommand.c_str(), unsentCommand.size());

        // if command send success, update unsent command.
        if(sentCount > 0){
          selectedServer->setUnsentCommand(unsentCommand.substr(sentCount, unsentCommand.size() - sentCount));
        }

        // if write complete or failed, change status to read.
        if((sentCount <= 0) || (selectedServer->getUnsentCommand() == "")){
          selectedServer->setStatus(F_READING);
          FD_CLR(selectedSocket, &ws);
          FD_SET(selectedSocket, &rs);
        }

      }
    }
    
  }
}



