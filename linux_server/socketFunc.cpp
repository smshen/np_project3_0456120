#ifndef SOCKETFUNC
#define SOCKETFUNC

#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

#include <netdb.h>
#include <stdio.h>

#define MAX_LENGTH_PER_CMD_LINE 15000

using namespace std;


int portbase = 0;

// create socket
int passivesock(char* service, const char* protocol, int qlen)
{
  struct servent *pse;
  struct protoent *ppe;
  struct sockaddr_in sin;
  int s, type;

  bzero((char*)&sin, sizeof(sin));
  sin.sin_family = AF_INET;
  sin.sin_addr.s_addr = INADDR_ANY;

  /* Map service name to port name*/
  if(pse = getservbyname(service, protocol))
    sin.sin_port = htons(ntohs((u_short)pse->s_port) + portbase);
  else if((sin.sin_port = htons((u_short)atoi(service))) == 0){
    exit(0);
  }

  /* Map protocol name to protocol number */
  if((ppe = getprotobyname(protocol)) == 0){
    exit(0);
  }

  /* Use protocol to choose a socket type */
  if(strcmp(protocol, "udp") == 0)
    type = SOCK_DGRAM;
  else
    type = SOCK_STREAM;

  /* Allocate a socket*/
  s = socket(PF_INET, type, ppe->p_proto);
  if(s < 0){
    cerr <<  "Socket error\n";
    exit(0);
  }

  /* Bind the socket */
  if(bind(s, (struct sockaddr*)&sin, sizeof(sin)) < 0){
    cerr << "Bind error\n";
    exit(0);
  }
  if(type == SOCK_STREAM && listen(s, qlen) < 0){
    cerr << "Listen error\n";
    exit(0);
  }
  return s;

}

// create a TCP socket
int passiveTCP(char* service, int qlen)
{
  return passivesock(service, "tcp", qlen);
}

char* readLineFromSocket(int newfd){
  int n         = -1;
  bool endofcmd = false;

  char buffer[MAX_LENGTH_PER_CMD_LINE];
  char* cmds = new char[MAX_LENGTH_PER_CMD_LINE];

  bzero(cmds, MAX_LENGTH_PER_CMD_LINE);
  while(!endofcmd){
    bzero(buffer, MAX_LENGTH_PER_CMD_LINE);
    if ((n =read(newfd, buffer, MAX_LENGTH_PER_CMD_LINE)) < 0) {
      cerr << "Read Error" << endl;
      return (char*)"fuck";
    }
    else{
      strcat(cmds, buffer);
      if(buffer[n-1] == '\n'){
        endofcmd = true;
      }
    }
  } 
  return cmds;
}

int connectsock(const char *host, const char *service, const char *protocol)
{
  struct hostent *phe;
  struct servent *pse;

  struct protoent *ppe;
  struct sockaddr_in sin;
  int s, type;

  bzero((char *)&sin, sizeof(sin));
  sin.sin_family = AF_INET;

  /* Map service name to port number */
  if(pse = getservbyname(service, protocol))
    sin.sin_port = pse->s_port;
  else if((sin.sin_port = htons((u_short)atoi(service))) == 0){
    cerr << "Port Error\n";
    return -1;
  }

  /* Map host name to IP address, allowing for dotted deciaml */
  if(phe = gethostbyname(host))
    bcopy(phe->h_addr, (char*)&sin.sin_addr, phe->h_length);
  else if((sin.sin_addr.s_addr = inet_addr(host)) == INADDR_NONE){
    cerr << "IP Error\n";
    return -1;
  }

  /* Map protocal name to protocol number */
  if((ppe = getprotobyname(protocol)) == 0){
    cerr << "Protocal Error\n";
    return -1;
  }
    
  /* Use protocol to choose a socket type */
  if(strcmp(protocol, "udp") == 0)
    type = SOCK_DGRAM;
  else
    type = SOCK_STREAM;

  /* Allocate a socket */
  s = socket(PF_INET, type, ppe->p_proto);
  if(s < 0){
    cerr << "Allocate socket Error\n";
    return -1;
  }


  /* Connect the socket */
  if(connect(s, (struct sockaddr *)&sin, sizeof(sin)) < 0){
    cerr << "Connect Error\n";
    return -1;
  }

  /* Set nonblock socket */
  int flag = fcntl(s, F_GETFL, 0);
  fcntl(s, F_SETFL, flag | O_NONBLOCK);

  return s;
}

int connectTCP(const char *host, const char *service)
{
  return connectsock(host, service, "tcp");
}

int connectUDP(const char *host, const char *service)
{
  return connectsock(host, service, "udp");
}


#endif
