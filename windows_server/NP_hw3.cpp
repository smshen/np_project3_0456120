#include <windows.h>
#include <iostream>
#include <list>
#include <fstream>
#include <sstream>
#include <regex>
#include <vector>
using namespace std;

#include "resource.h"
#include "RequestedServer.h"

#define SERVER_PORT 7798

#define WM_SOCKET_NOTIFY (WM_USER + 1)

#define MAXBUFFERSIZE 1000

BOOL CALLBACK MainDlgProc(HWND, UINT, WPARAM, LPARAM);
int EditPrintf (HWND, TCHAR *, ...);

// self defined functions
string getHttpRequest(int);
string readFile2String(string);
void handleRequest(int, string, HWND);
vector<string> split(string, char);
void send2Socket(int, string);
int connect2Server(const char*, int, HWND);
string cleanString(string);
void sendCGITemplate(int);
void handleReadFDEvent(int, WPARAM, HWND, HWND);
void handleWriteFDEvent(int, WPARAM, HWND, HWND);
void handleSocketClose(WPARAM, HWND);
void write2Browser(int, int , string);
//=================================================================
//	Global Variables
//=================================================================
list<SOCKET> Socks;
vector<RequestedServer> serverList;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, int nCmdShow)
{
	
	return DialogBox(hInstance, MAKEINTRESOURCE(ID_MAIN), NULL, MainDlgProc);
}

BOOL CALLBACK MainDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
	WSADATA wsaData;

	static HWND hwndEdit;
	static SOCKET msock, ssock;
	static struct sockaddr_in sa;

	int err;

	string httpRequest;
	string successResponseMsg = "HTTP/1.1 200 OK\r\n";

	switch(Message) 
	{
		case WM_INITDIALOG:
			hwndEdit = GetDlgItem(hwnd, IDC_RESULT);
			break;
		case WM_COMMAND:
			switch(LOWORD(wParam))
			{
				case ID_LISTEN:

					WSAStartup(MAKEWORD(2, 0), &wsaData);

					//create master socket
					msock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

					if( msock == INVALID_SOCKET ) {
						EditPrintf(hwndEdit, TEXT("=== Error: create socket error ===\r\n"));
						WSACleanup();
						return TRUE;
					}

					err = WSAAsyncSelect(msock, hwnd, WM_SOCKET_NOTIFY, FD_ACCEPT | FD_CLOSE | FD_READ | FD_WRITE);
					//err = WSAAsyncSelect(msock, hwnd, WM_SOCKET_NOTIFY, FD_ACCEPT);

					if ( err == SOCKET_ERROR ) {
						EditPrintf(hwndEdit, TEXT("=== Error: select error ===\r\n"));
						closesocket(msock);
						WSACleanup();
						return TRUE;
					}

					//fill the address info about server
					sa.sin_family		= AF_INET;
					sa.sin_port			= htons(SERVER_PORT);
					sa.sin_addr.s_addr	= INADDR_ANY;

					//bind socket
					err = bind(msock, (LPSOCKADDR)&sa, sizeof(struct sockaddr));

					if( err == SOCKET_ERROR ) {
						EditPrintf(hwndEdit, TEXT("=== Error: binding error ===\r\n"));
						WSACleanup();
						return FALSE;
					}

					err = listen(msock, 2);
		
					if( err == SOCKET_ERROR ) {
						EditPrintf(hwndEdit, TEXT("=== Error: listen error ===\r\n"));
						WSACleanup();
						return FALSE;
					}
					else {
						EditPrintf(hwndEdit, TEXT("=== Server START ===\r\n"));
					}

					break;
				case ID_EXIT:
					EndDialog(hwnd, 0);
					break;
			};
			break;

		case WM_CLOSE:
			EndDialog(hwnd, 0);
			break;

		case WM_SOCKET_NOTIFY:
			switch( WSAGETSELECTEVENT(lParam) )
			{
				case FD_ACCEPT:
					ssock = accept(msock, NULL, NULL);
					Socks.push_back(ssock);
					EditPrintf(hwndEdit, TEXT("=== Accept one new client(%d), List size:%d ===\r\n"), ssock, Socks.size());
					// get the http request path string
					httpRequest = getHttpRequest(ssock);
					EditPrintf(hwndEdit, TEXT("%s"), httpRequest.c_str());
					
					send2Socket(ssock, successResponseMsg);

					EditPrintf(hwndEdit, TEXT("valid server %d\r\n"), serverList.size());

					if (httpRequest != "") {
						handleRequest(ssock, httpRequest, hwnd);

						int i;
						for (int i = 0; i < serverList.size(); i++)
						{
							RequestedServer server = serverList[i];
							EditPrintf(hwndEdit, TEXT("valid server %s\r\n"), server.getIP().c_str());
						}
					}
					
					//closesocket(ssock);
					break;
				case FD_READ:
				//Write your code for read event here.
					handleReadFDEvent(ssock, wParam, hwnd, hwndEdit);
					break;
				case FD_WRITE:					
				//Write your code for write event here
					handleWriteFDEvent(ssock, wParam, hwnd, hwndEdit);
					break;
				case FD_CLOSE:
					EditPrintf(hwndEdit, TEXT("Socket close\n\n"));
					handleSocketClose(wParam, hwnd);
					break;
			};
			break;
		
		default:
			return FALSE;


	};

	return TRUE;
}

int EditPrintf (HWND hwndEdit, TCHAR * szFormat, ...)
{
     TCHAR   szBuffer [1024] ;
     va_list pArgList ;

     va_start (pArgList, szFormat) ;
     wvsprintf (szBuffer, szFormat, pArgList) ;
     va_end (pArgList) ;

     SendMessage (hwndEdit, EM_SETSEL, (WPARAM) -1, (LPARAM) -1) ;
     SendMessage (hwndEdit, EM_REPLACESEL, FALSE, (LPARAM) szBuffer) ;
     SendMessage (hwndEdit, EM_SCROLLCARET, 0, 0) ;
	 return SendMessage(hwndEdit, EM_GETLINECOUNT, 0, 0); 
}

string getHttpRequest(int ssock)
{
	char buffer[MAXBUFFERSIZE];
	memset(buffer, 0, sizeof buffer);
	recv(ssock, buffer, MAXBUFFERSIZE, 0);
	
	// get http request
	// remove line break in http request.
	regex cleanRegex("[\r\n]");
	string requestString = string(buffer);
	requestString = regex_replace(requestString, cleanRegex, "");

	// get request parameter by regex.
	regex re("^(\\w+) ([\\/\\w\\.\\?\\=\\&]*) (HTTP\\/\\d\\.\\d).*");
	smatch sm;
	regex_match(requestString, sm, re);


	// cerr << requestString << endl;
	if (sm.size() < 4)
		return "";

	
	return string(sm[2]);
}

// read html file to string.
string readFile2String(string fileName)
{
	ifstream fileStream = ifstream(fileName);
	// read whole file content by ifstream.
	string content((istreambuf_iterator<char>(fileStream)),
		(istreambuf_iterator<char>()));
	fileStream.close();
	return content;
}

vector<string> split(string str, char delimiter) {
	vector<string> internal;
	// Turn the string into a stream.
	stringstream ss(str);
	string tok;

	while (getline(ss, tok, delimiter)) {
		internal.push_back(tok);
	}
	return internal;
}

// send message to sock.
void send2Socket(int ssock, string msg)
{
	send(ssock, msg.c_str(), msg.size(), 0);
}

int connect2Server(const char *hostName, int port, HWND hwnd)
{
	int sockfd;
	int flag, err;
	char hostname[100];
	struct hostent *hostinfo;
	struct sockaddr_in  serv_addr;

	strcpy(hostname, hostName);

	if ((hostinfo = gethostbyname(hostname)) == NULL) {
		//EditPrintf(hwndEdit, TEXT("=== serverConnect: gethostbyname() failed: %s ===\r\n"), hostname);
		return -1;
	}

	//bzero((char *)&serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr = *((struct in_addr *)hostinfo->h_addr);
	serv_addr.sin_port = htons(port);

	sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sockfd == INVALID_SOCKET) {
		//EditPrintf(hwndEdit, TEXT("=== serverConnect: socket() failed ===\r\n"));
		return -2;
	}

	err = WSAAsyncSelect(sockfd, hwnd, WM_SOCKET_NOTIFY, FD_CONNECT | FD_READ | FD_CLOSE);
	if (err == SOCKET_ERROR) {
		//EditPrintf(hwndEdit, TEXT("=== serverConnect: Select() failed ===\r\n"));
		closesocket(sockfd);
		return -3;
	}

	if (connect(sockfd, (SOCKADDR *)&serv_addr, sizeof(serv_addr)) == SOCKET_ERROR) {
		if (WSAGetLastError() != WSAEWOULDBLOCK) {
			//EditPrintf(hwndEdit, TEXT("=== serverConnect: connect() failed ===\r\n"));
			closesocket(sockfd);
			return -4;
		}
		//EditPrintf(hwndEdit, TEXT("=== serverConnect: connect() temporarily failed ===\r\n"));
	}

	//EditPrintf(hwndEdit, TEXT("=== serverConnect: Returning socket ===\r\n"));
	return sockfd;
}

// replace '\r' to ''.
string cleanString(string targetString)
{

	regex e1("[\r]");
	return regex_replace(targetString, e1, "");

}

void write2Browser(int ssock, int id, string msg)
{
	string tempString;

	// replace '>' to '&gt' for html syntax.
	regex e1(">");
	tempString = regex_replace(cleanString(msg), e1, "&gt");

	// replace '<' to '&lt' for html syntax.
	regex e2("<");
	tempString = regex_replace(tempString, e2, "&lt");

	// replace '\n' to '<br>' for html syntax.
	regex e3("[\n]");
	tempString = regex_replace(tempString, e3, "<br>");

	send2Socket(ssock, "<script>document.all[\'m" + to_string(id) + "\'].innerHTML += \"" + tempString + "\";</script>");

}


void sendCGITemplate(int ssock)
{
  send2Socket(ssock, "<html>");
  send2Socket(ssock, "<head>");
  send2Socket(ssock, "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=big5\" />");
  send2Socket(ssock, "<title>Network Programming Homework 3</title>");
  send2Socket(ssock, "</head>");
  send2Socket(ssock, "<body bgcolor=#336699>");
  send2Socket(ssock, "<font face=\"Courier New\" size=2 color=#FFFF99>");
  send2Socket(ssock, "<table width=\"800\" border=\"1\">");

  // write table header.
  send2Socket(ssock, "<tr>");
  int i;
  for (i = 0; i < serverList.size(); ++i) {
	send2Socket(ssock, "<td>" + serverList[i].getIP() + "</td>");
  }

  send2Socket(ssock, "</tr><tr>");

  // write table content.
  for (i = 0; i < serverList.size(); ++i) {
	  send2Socket(ssock, "<td valign=\"top\" id=\"m" + to_string(serverList[i].getID()) + "\"></td>");
  }
  send2Socket(ssock, "</tr></table>");
}

void handleReadFDEvent(int ssock, WPARAM wParam, HWND hwnd, HWND hwndEdit)
{
	for (int i = 0; i < serverList.size(); i++)
	{
		RequestedServer *server = &(serverList[i]);
		// handle the event by corresbonding server.
		if (server->getSocketfd() == wParam) {
			
			int n;
			char buffer[MAXBUFFERSIZE];
			// clear buffer.
			memset(buffer, '\0', sizeof(buffer));
			n = recv(server->getSocketfd(), buffer, MAXBUFFERSIZE, 0);
			EditPrintf(hwndEdit, TEXT("Server %d has read %d characters\r\n"), server->getID(), n);

			string recvMsg = string(buffer);
			write2Browser(server->getOriginalSocket(), server->getID(), recvMsg);

			// if server receive promote from remote server, change status to write
			if (recvMsg.find("% ") != string::npos) {
				//send(server.getSocketfd(), "exit\n", 5, 0);
				EditPrintf(hwndEdit, TEXT("Server %d change status to write\r\n"), server->getID());
				int err = WSAAsyncSelect(server->getSocketfd(), hwnd, WM_SOCKET_NOTIFY, FD_WRITE);
			}
		}
	}
	return;
}

void handleWriteFDEvent(int ssock, WPARAM wParam, HWND hwnd, HWND hwndEdit)
{
	for (int i = 0; i < serverList.size(); i++)
	{
		RequestedServer *server = &(serverList[i]);
		// handle the event by corresbonding server.
		if (server->getSocketfd() == wParam) {
			EditPrintf(hwndEdit, TEXT("Socket %d is writing\r\n"), server->getID());
			// check command is sent or not.
			if (server->getUnsentCommand() == "") {
				// pop a new command if there is exist more command.
				if (!server->hasCommand()) {
					int err = WSAAsyncSelect(server->getSocketfd(), hwnd, WM_SOCKET_NOTIFY, FD_READ | FD_CLOSE);
					//closesocket(server->getOriginalSocket());
					//closesocket(server->getSocketfd());
				}
			}

			// send command
			string unsentCommand = server->getUnsentCommand();
			write2Browser(server->getOriginalSocket(), server->getID(),cleanString(unsentCommand));
			int sentCount = send(server->getSocketfd(), unsentCommand.c_str(), unsentCommand.size(), 0);

			// update unsent command.
			if (sentCount > 0) {
				server->setUnsentCommand(unsentCommand.substr(sentCount, unsentCommand.size() - sentCount));
			}

			// change status to read
			if (sentCount <= 0 || (server->getUnsentCommand() == "")) {
				int err = WSAAsyncSelect(server->getSocketfd(), hwnd, WM_SOCKET_NOTIFY, FD_READ | FD_CLOSE);
			}
		}
	}
	return;
}

void handleSocketClose(WPARAM wParam, HWND hwnd)
{
	for (vector<RequestedServer>::iterator it = serverList.begin(); it != serverList.end();++it)
	{
		if (it->getSocketfd() == wParam) {
			int err = WSAAsyncSelect(it->getSocketfd(), hwnd, WM_SOCKET_NOTIFY, 0);
			// close socket of remote server.
			closesocket(it->getSocketfd());
			
			// if the request is the last, close the browser socket.
			if (serverList.size() == 1) {
				closesocket(it->getOriginalSocket());
			}
			serverList.erase(it);
			break;
		}

	}
}

void handleRequest(int ssock, string requestString, HWND hwnd)
{
	string contentTypeHeader = "Content-Type:text/html;charset=big5\n\n";

	string requestFilePath;
	string queryParameterString;

	// parse request string
	// if request has parameter(GET) get the parameters
	if (requestString.find("?") != string::npos) {
		vector<string> parameter = split(requestString, '?');
		requestFilePath = parameter[0];
		queryParameterString = parameter[1];
	}
	else {
		// get the file name without parameteres.
		requestFilePath = requestString;
		queryParameterString = "";
	}

	// get the vice file name.
	vector<string> fileInfo = split(requestFilePath, '.');

	// add local path to the file.
	requestFilePath = "." + requestFilePath;
	cerr << requestFilePath << endl;

	if (fileInfo[1] == "html") {
		string htmlContent = readFile2String(requestFilePath);

		send2Socket(ssock, contentTypeHeader);
		send2Socket(ssock, htmlContent);
		closesocket(ssock);
	}
	else if (fileInfo[1] == "cgi") {
		if (requestFilePath == "./hw3.cgi") {
			serverList.clear();
			send2Socket(ssock, contentTypeHeader);
			// use regex to get server information.
			regex re("h1=(.*)&p1=(.*)&f1=(.*)&h2=(.*)&p2=(.*)&f2=(.*)&h3=(.*)&p3=(.*)&f3=(.*)&h4=(.*)&p4=(.*)&f4=(.*)&h5=(.*)&p5=(.*)&f5=(.*)");
			smatch sm;

			// get match string by regular expression.
			regex_match(queryParameterString, sm, re);

			int i;
			// retrieve server information from query string
			for (i = 0; i < sm.size(); ++i) {
				if ((i % 3) == 1) {
					// get the information of server.
					int id = i / 3;
					string ip = sm[i];
					string port = sm[++i];
					string fileName = "./test/" + string(sm[++i]);


					// if no missing information, create a RequestedServer object.
					if (ip != "" && port != "" && fileName != "") {
						RequestedServer server = RequestedServer(ssock, id, ip, port, fileName);

						// check file exist or not
						ifstream fileStream = ifstream(fileName.c_str());
						if (fileStream.is_open()) {
							fileStream.close();
							// read content of file and add to server object.
							string content = readFile2String(fileName);
							server.setFileContent(content);

							// connect to server.
							int sockfd;
							// check server is available
							if ((sockfd = connect2Server(ip.c_str(), stoi(port), hwnd)) > 0)
							{
								server.setSocketfd(sockfd);
								// if socket is valid, push to monitoring list.
								serverList.push_back(server);
							}				
						}

					}
				}
			}

			sendCGITemplate(ssock);
		}
	}
}
