#ifndef REQUESTEDSERVER_H
#define REQUESTEDSERVER_H

#include <string>
#include <list>

using namespace std;

class RequestedServer
{
public:

  RequestedServer (int, int, string, string, string);
  // virtual ~RequestedServer ();
  void            setSocketfd(int);
  int             getSocketfd();
  void            setFileContent(string);
  string          getFileContent();
  int             getID();
  int             getOriginalSocket();
  string          getIP();
  void            setUnsentCommand(string);
  string          getUnsentCommand();
  void            setStatus(int);
  int             getStatus();
  void            showInfo();
  list<string>    split(string, char);
  int             hasCommand();

private:
  int          _originalSocket;
  int          _id;
  string       _ip;
  string       _port;
  string       _fileName;
  string       _fileContent;
  list<string> _commandList;
  string       _currCommand;
  string       _unsentCommand;
  int          _socketfd;
  int          _status;

};

#endif
